import string
translator = str.maketrans('', '', string.punctuation)
str = input()
str = str.upper()
dict_str = {}

new_string = (str.translate(translator))

str_list = new_string.split()
unique_words = set(str_list)

for words in unique_words:
    dict_str[words] = str_list.count(words)
sorted_list = sorted(dict_str.items(), key = lambda item: item[1], reverse = True)
##for keys in sorted_list:
##    print(keys[0]," ", keys[1])

total_count = 0
longest_word_len = 0
for keys in sorted_list:
    if len(keys[0]) > longest_word_len:
        longest_word_len = len(keys[0])
    total_count += keys[1]
for keys in sorted_list:
    percentage = int(keys[1]/total_count*100)
    print("{1:{0}}".format(longest_word_len+5, keys[0]), "[***]", "{1:{0}}".format(2, percentage),"%")